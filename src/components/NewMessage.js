import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Giphy from './Giphy';



export class NewMessage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: null,
            image: null,
        }
        this.handleClose = this.handleClose.bind(this);
        this.handlePost = this.handlePost.bind(this);
        this.handleText = this.handleText.bind(this);
        this.handleImageClick = this.handleImageClick.bind(this);
        this.popover = (
            <Popover id="popover-basic">
                <Popover.Body>
                    <Giphy click={this.handleImageClick}></Giphy>
                </Popover.Body>
            </Popover>
        );
    }

    handleImageClick(url){
        this.setState({image: url});
        document.body.click()
    }

    handleText(e) {
        this.setState({ text: e.target.value === '' ? null : e.target.value })
    }

    handleClose() {
        this.props.handleClick();
    }

    handlePost() {
        if (!this.state.text && !this.state.image) {
            console.log("return")
            return
        }
        let data = {
            text: this.state.text,
            image: this.state.image,
        }
        this.props.post(data);
        this.handleClose()
    }

    render() {
        const {image} = this.state
        return (
            <div>
                <Modal show={true} onHide={this.handleClose} enforceFocus={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Post Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Control as="textarea" rows={3} onChange={this.handleText} placeholder='Type your new message' />
                        </Form.Group>
                        {image && image !== ''? (<img className='added-gif' alt='' src={image}></img>): ''}
                        </Modal.Body>
                    <Modal.Footer className='justify-content-between' >
                        <OverlayTrigger rootClose={true} trigger="click" placement="bottom" overlay={this.popover}>
                            <Button variant="secondary" >
                                Gifs
                            </Button>
                        </OverlayTrigger>

                        <Button variant="primary" onClick={this.handlePost}>
                            Post
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default NewMessage

