import React, { Component } from 'react'

export class Navbar extends Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this); 
    }

    handleClick() {
        this.props.handleClick()
    }

    render() {
        return (
            <nav className="navbar sticky-top">
                <div className="container">
                    <a className="navbar-brand mr-auto" href="home">
                        <svg width="13.415" height="25.047" viewBox="0 0 13.415 25.047"><defs></defs><path className="fb-svg" strokeWidth={'10px'} d="M35.426,14.089l.7-4.533h-4.35V6.615a2.267,2.267,0,0,1,2.556-2.449h1.977V.306A24.114,24.114,0,0,0,32.795,0c-3.582,0-5.923,2.171-5.923,6.1V9.556H22.89v4.533h3.982V25.047h4.9V14.089Z" transform="translate(-22.89)" /></svg>
                    </a>
                    <ul className="nav navbar-nav navbar-right">
                        <li >
                            <button className="btn btn-sm btn-outline-light" type="button" id='new-message' onClick={(e)=>this.handleClick(e)}>New Message</button>
                        </li>
                    </ul>


                </div>
            </nav>

        )
    }
}

export default Navbar
