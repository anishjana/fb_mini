import React, { Component } from 'react'
import Messages from './Messages';
import Navbar from './Navbar'
import NewMessage from './NewMessage';



export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [
                {
                    text: 'Hello',
                    image: 'https://developers.giphy.com/branch/master/static/why_2-7d9f0b8b01f60835fa11cee8f3ec8978.gif'
                },
                {
                    text: 'this is post no 221',
                    image: 'https://developers.giphy.com/branch/master/static/why_1-bc314dfb5742fb64adbf0af3d7c8b8ed.gif'
                },
                {
                    text: 'first post'
                }
            ],
            openmodal: false,
        }

        this.toggleModal = this.toggleModal.bind(this);
        this.postMessage = this.postMessage.bind(this);
    }

    toggleModal() {
        this.setState({openmodal: !this.state.openmodal})
    }

    postMessage(data){
        let messages = this.state.messages;
        messages.unshift(data);
        this.setState({'messages': messages})
    }

    render() {
        const {openmodal} = this.state;
        return (
            <div>
                <Navbar handleClick={this.toggleModal}></Navbar>
                {openmodal? (<NewMessage handleClick={this.toggleModal} post={this.postMessage}></NewMessage>):''}
                <div className="container">
                    <div className="row justify-content-center ">
                        <div className="col col-lg-8 col-md-8 mb-3 align-self-center ">
                            <Messages messages={this.state.messages}></Messages>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home
