import React, { Component } from 'react'

export class Messages extends Component {
    constructor(props) {
        super(props)
        this.state = {
            messages: []
        }
    }

    // componentDidUpdate() {
    //     this.setState({ messages: this.props.messages })
    // }

    componentDidMount() {
        this.setState({ messages: this.props.messages })
    }


    render() {
        const { messages } = this.state;

        return (
            <div>
                {messages.map((message, index) => {
                    return (
                        <div key={index} className="card mt-3" style={{ width: '100%' }}>

                            <div className="card-body pb-2">
                                <div className="card-top">
                                    <img src="/user.png" className="rounded-circle" alt="" width={20} height={20} />
                                    <h6 className="card-title">User</h6>
                                </div>
                                <p className="card-text ml-2_5 mt-1">{message.text}</p>
                            </div>
                            {message.image ? (<img className="card-img-bottom ml-2_5" src={message.image} alt="QA"  />) : ''}
                            <br></br>
                        </div>
                    )
                }
                )}
            </div>
        )
    }
}

export default Messages
