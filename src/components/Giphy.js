import React, { Component } from 'react'

export class Giphy extends Component {
    constructor(props) {
        super(props)

        this.state = {
            search: '',
            gifs: []
        }
        this.handleSearch = this.handleSearch.bind(this);
        this.handleClick = this.handleClick.bind(this)
        this.hitGiphy = this.hitGiphy.bind(this);
        this.timer = null;
    }

    handleClick(e,url) {
        this.props.click(url)
    }

    handleSearch(e) {
        let value = e.target.value
        this.setState({ search: value })
        let url = value !== '' ? `https://api.giphy.com/v1/gifs/search?api_key=${process.env.REACT_APP_GIPHY_API_KEY}&limit=3&q=${value}` : `https://api.giphy.com/v1/gifs/trending?api_key=${process.env.REACT_APP_GIPHY_API_KEY}&limit=3`
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.hitGiphy(url);
        }, 2000);
    }

    async hitGiphy(url) {
        await fetch(url)
            .then(response => response.json())
            .then(resp => {
                let arr = []
                resp.data.forEach(item => {
                    let gif = item.images.downsized.url;
                    arr.push(gif)
                    this.setState({ 'gifs': arr })
                });

            })
            .catch(err => {
                console.error(err);
            });
    }

    async componentDidMount() {
        let url = `https://api.giphy.com/v1/gifs/trending?api_key=${process.env.REACT_APP_GIPHY_API_KEY}&limit=3`
        await this.hitGiphy(url);
    }

    render() {
        const { gifs } = this.state;
        return (
            <div style={{ height: '100%' }}>
                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="Search Gifs" aria-label="Username" aria-describedby="basic-addon1" onChange={this.handleSearch} />
                </div>
                <div id='grid' style={{ height: '80%' }}>
                    {gifs.map((gif,index) => {
                        return (<img key={index} className='gifs' alt='' src={gif} onClick={(e)=>this.handleClick(e,gif)}></img>)
                    })}
                </div>
                    <br></br>
            </div>


        )
    }
}

export default Giphy
