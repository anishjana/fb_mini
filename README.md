# FB_Mini

This project was bootstrapped with [React](https://github.com/facebook/create-react-app),[Giphy](https://developers.giphy.com/) and [React Bootstrap](https://react-bootstrap.github.io/) .

## Demo
[https://fb-mini.netlify.app/](https://fb-mini.netlify.app/)

## Available Scripts

After cloning, in the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3006](http://localhost:3006) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

## Steps/Usage

1. Click on 'New Message' button on navbar.
2. Type something in textarea or keep it empty.
3. Click on 'Gif' button to open up trending gifs.
4. Type in Search bar to get related Gifs. (Avoid searching needlesly and continuously, API got limited hits)
5. Click on any GIF to add.
6. Hit 'Post' button.

